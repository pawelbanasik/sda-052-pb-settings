package com.pawelbanasik.rent.sda_052_pb_preferences_android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.text_view_settings)
    protected TextView textViewSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_preferences)
    protected void clickPreferencesButton() {
        Intent intent = new Intent(this, PreferencesActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String first_player_name = sharedPreferences.getString("edit_text_preference_1", "");
        String second_player_name = sharedPreferences.getString("edit_text_preference_2", "");
        Boolean isXFirstPlayer = sharedPreferences.getBoolean("switch_preference_1", false);
        Boolean isNewGameStarted = sharedPreferences.getBoolean("check_box_preference_1", false);

        String preferences = first_player_name + second_player_name + String.valueOf(isXFirstPlayer) + String.valueOf(isNewGameStarted);
        textViewSettings.setText(preferences);
    }
    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }


}
