package com.pawelbanasik.rent.sda_052_pb_preferences_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceFragmentCompat;

public class PreferencesActivity extends AppCompatActivity {

//    public static final String KEY_USER_NAME = "key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferencesFragment())
                .commit();

    }

    public static class PreferencesFragment extends PreferenceFragmentCompat // implements SharedPreferences.OnSharedPreferenceChangeListener{
    {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.preferences, rootKey);
        }

//        @Override
//        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
//            if (key.equals(KEY_USER_NAME)){
//                findPreference(KEY_USER_NAME).setSummary(sharedPreferences.getString(KEY_USER_NAME, ""));
//            }
//        }
    }

}
